package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;



public class CreateLeadPage extends Annotations{ 

	public CreateLeadPage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how=How.ID, using="createLeadForm_companyName") 
	WebElement eleCompName;
	public CreateLeadPage typeCompanyName(String cName) {
		clearAndType(eleCompName, cName);
		return this;
	}
	@FindBy(how=How.ID, using="createLeadForm_firstName") 
	WebElement eleFirstName;
	public CreateLeadPage typeFirstName(String fName) {
		clearAndType(eleFirstName, fName);
		return this;
	}
	
	@FindBy(how=How.ID, using="createLeadForm_lastName") 
	WebElement eleLastName;
	public CreateLeadPage typeLastName(String lName) {
		clearAndType(eleLastName, lName);
		return this;
	}
	
	@FindBy(how=How.CLASS_NAME, using="smallSubmit") 
	WebElement eleCreateLeadBtn;
	public ViewLeadPage clickCreateLeadButton() {
		click(eleCreateLeadBtn);
		return new ViewLeadPage();
	}
	
	
	
	
}


