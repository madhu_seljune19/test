package com.leafBot.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.leafBot.testng.api.base.Annotations;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.When;




public class ViewLeadPage extends Annotations{ 

	public ViewLeadPage() {
		PageFactory.initElements(driver, this); 
	}
	@CacheLookup
	@FindBy(how=How.ID, using="viewLead_firstName_sp") 
	WebElement eleFirstNameText;
	public ViewLeadPage verifyFirstName() 
	{
		String text = getElementText(eleFirstNameText);
		if(text.equals("Sethu")) {
			System.out.println("First name matches with input data");
		}else {
			System.err.println("First name not matches with input data");
		}
		return this;
	}
	
	
}


